﻿// Konfigurace
const path = require("path");
process.env["NODE_CONFIG_DIR"] = path.resolve(__dirname, "../env");

const express = require("express");
const app = express();
const { JsonRoute } = require("json-routing");
const config = require("config");
const port = config?.port || 8080;

app.use(express.json());

// Nastavení JsonRoute
// https://gimox.github.io/json-routing/
new JsonRoute(app, {
  routesPath: "./config",
  processdir: __dirname,
  jwt: {
    secret: "masterOfAlpacas",
  },
  policyPath: "./middleware",
}).start();

// Middleware na procesování chyb
app.use((err, req, res, next) => {
  res.status(err.status ? err.status : 400).json({ error: err, stack: err.stack });
});

// Start aplikace
app.listen(port, () => {
  console.log("Server poslouchá na portu " + port);
});
