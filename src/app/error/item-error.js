const CreateItem = {
  InvalidDtoIn: class extends Error {
    constructor(params, cause) {
      super();
      this.code = `item/create/invalidDtoIn`;
      this.message = "DtoIn is not valid.";
      this.paramMap = params;
      this.status = 400;
    }
  },
};

module.exports = { CreateItem };
