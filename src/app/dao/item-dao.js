const BaseDao = require("./base-dao");
const { ObjectId } = require("mongodb");

class ItemDao extends BaseDao {
  constructor() {
    super("items");
  }

  async insertOne(newItem) {
    return super.insertOne(newItem);
  }

  async find(dtoIn) {
    let filter = {
      _id: new ObjectId(dtoIn.id)
    };
    return super.findOne(filter);
  }

  async list(dtoIn) {}

  async updateOne(updateItem) {}
}

module.exports = ItemDao;
