const { MongoClient } = require("mongodb");
const config = require("config");
const dbConfig = config.get("dbConfig");
const client = new MongoClient(dbConfig.uri);

const DATABASE_NAME = dbConfig.name;

class BaseDao {
  constructor(collectionName) {
    this.collectionNamme = collectionName;
  }
  async getCollection() {
    await client.connect();
    const database = client.db(DATABASE_NAME);
    return database.collection(this.collectionNamme);
  }

  async createIndex(index, unique) {
    try {
      let collection = await this.getCollection();
      await collection.createIndex(index, unique);
    } finally {
      await client.close();
    }
  }

  async insertOne(doc) {
    let result;
    try {
      let collection = await this.getCollection();
      result = await collection.insertOne(doc);
    } finally {
      await client.close();
    }
    return result;
  }

  async replaceOne(doc) {
    let result;
    try {
      let collection = await this.getCollection();
      let filter;
      if (doc._id) {
        filter = { _id: doc._id };
      }
      const options = { upsert: true };
      result = await collection.replaceOne(filter, doc, options);
    } finally {
      await client.close();
    }
    return result;
  }

  async findOne(filter) {
    let result;
    try {
      let collection = await this.getCollection();
      result = await collection.findOne(filter);
    } finally {
      await client.close();
    }
    return result;
  }

  async find(filter) {
    let result;
    try {
      let collection = await this.getCollection();
      result = await collection.find(filter).toArray();
    } finally {
      await client.close();
    }
    return result;
  }

  async deleteOne(filter) {
    let result;
    try {
      let collection = await this.getCollection();
      result = await collection.deleteOne(filter);
    } finally {
      await client.close();
    }
    return result;
  }

  async updateOne(filter, query) {
    let result;
    try {
      let collection = await this.getCollection();
      result = await collection.updateOne(filter, query);
    } finally {
      await client.close();
    }
    return result;
  }
}
module.exports = BaseDao;
