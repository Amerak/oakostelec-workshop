const ItemModel = require("../model/item-model");

class ItemController {
  async createItem(req, res, next) {
    await ItemModel.createItem(req.body)
      .then((dtoOut) => {
        res.json(dtoOut);
      })
      .catch((e) => {
        console.error(e);
        next(e);
      });
  }

  async getItem(req, res, next) {
    await ItemModel.getItem(req.body)
      .then((dtoOut) => {
        res.json(dtoOut);
      })
      .catch((e) => {
        console.error(e);
        next(e);
      });
  }
}

module.exports = new ItemController();
