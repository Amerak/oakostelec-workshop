const validate = require("validate.js");
const normalizedPath = require("path").join(__dirname, "../validation_schema");
const schemas = require("require-all")({
  dirname: normalizedPath,
});
const moment = require("moment");

/**
 * Custom datetime validation function
 */
validate.extend(validate.validators.datetime, {
  parse: function (value, options) {
    return +moment.utc(value);
  },

  format: function (value, options) {
    let format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
    return moment.utc(value).format(format);
  },
});

/**
 * Helper for input validations
 */
class ValidationHelper {
  /**
   *
   * @param {Object} dtoIn object to validate
   * @param {String} schemaName name of validation schema
   * @returns {Promise<{validationResult: any, isValid: (function(): boolean), cleanDtoIn: any}>}
   */
  async validate(dtoIn, schemaName) {
    let validationSchema;
    Object.keys(schemas).forEach((key) => {
      if (schemas[key][schemaName]) {
        validationSchema = schemas[key][schemaName];
      }
    });
    let cleanDtoIn = validate.cleanAttributes(dtoIn, validationSchema);
    let validationResult = validate(cleanDtoIn, validationSchema);

    return {
      cleanDtoIn,
      validationResult,
      isValid: () => {
        return !validationResult;
      },
    };
  }
}

module.exports = new ValidationHelper();
