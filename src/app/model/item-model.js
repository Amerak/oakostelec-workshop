const ItemDao = require("../dao/item-dao");
const DtoValidator = require("../helper/validation-helper");
const ItemError = require("../error/item-error");

class ItemModel {
  constructor() {
    this.ItemDaoInstance = new ItemDao();
  }

  async createItem(dtoIn) {
    let result= await DtoValidator.validate(dtoIn, "itemCreateDtoInType");
    if(!result.isValid()){
      throw new ItemError.CreateItem.InvalidDtoIn(result.validationResult);
    }

    return await this.ItemDaoInstance.insertOne(result.cleanDtoIn);
  }

  async getItem(dtoIn) {
    return await this.ItemDaoInstance.find(dtoIn);
  }

}

module.exports = new ItemModel();
