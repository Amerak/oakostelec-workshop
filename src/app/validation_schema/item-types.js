// https://validatejs.org/#validators-type
const itemCreateDtoInType = {
  name: {
    type: "string",
    presence: true,
  },
  description: {
    type: "string",
  },
  duration: {
    type: "integer",
  },
};

module.exports = {
  itemCreateDtoInType,
};
