module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
    jest: true,
  },
  parser: require.resolve("@babel/eslint-parser"),
  parserOptions: {
    requireConfigFile: false,
  },
  plugins: ["json", "prettier"],
  extends: ["eslint:recommended", "prettier", "plugin:json/recommended"],
  rules: {
    "no-const-assign": "warn",
    "no-this-before-super": "warn",
    "no-undef": "warn",
    "no-unreachable": "warn",
    "no-unused-vars": ["warn", { args: "none", ignoreRestSiblings: true }],
    "no-use-before-define": ["warn", { functions: false }],
    "constructor-super": "warn",
    "valid-typeof": "warn",
    "no-console": "off",
    "prettier/prettier": "warn",
  },
};
